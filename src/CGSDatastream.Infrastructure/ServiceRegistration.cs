﻿using LCP.OCTODashboard.CGSDatastream.Application.Interfaces.Data;
using LCP.OCTODashboard.CGSDatastream.Application.Interfaces.Files;
using LCP.OCTODashboard.CGSDatastream.Application.Interfaces.Storage;
using LCP.OCTODashboard.CGSDatastream.Infrastructure.Services.Data;
using LCP.OCTODashboard.CGSDatastream.Infrastructure.Services.Files;
using LCP.OCTODashboard.CGSDatastream.Infrastructure.Services.Storage;
using Microsoft.Extensions.DependencyInjection;

namespace LCP.OCTODashboard.CGSDatastream.Infrastructure
{
    public static class ServiceRegistration
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services)
        {
            services
                .AddTransient<IStorageService, AzureStorageService>()
                .AddTransient<ICgsDataService, CgsDataService>()
                .AddTransient<IAwsStorageService, AwsStorageService>()
                .AddTransient<IParquetFileService, ParquetFileService>()
                .AddTransient<IDatastreamService, DatastreamService>();

            return services;
        }
    }
}