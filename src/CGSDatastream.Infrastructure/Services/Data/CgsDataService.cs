﻿using CGSDatastream.Application.Common.Models;
using LCP.OCTODashboard.CGSDatastream.Application.Interfaces.Data;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LCP.OCTODashboard.CGSDatastream.Infrastructure.Services.Data
{
    public class CgsDataService : ICgsDataService
    {
        public SqlConnection _sqlConnection;
        private readonly List<string> invalidSQLKeys = new List<string> { ";", "--", "/*", "*/", "xp_" };
        private readonly string _connString;

        public CgsDataService()
        {
            _connString = Environment.GetEnvironmentVariable("LCP_DB_CONNECTION_STRING");
        }

        public async Task<DataResponseModel> GetData(DataRequestModel request, CancellationToken cancellationToken)
        {
            //set new empty data tabl
            var dataTable = new DataTable();

            //int data response model
            var dataResponse = new DataResponseModel
            {
                Data = dataTable,
                Runtime = 0.0
            };

            //set new SQL connection
            _sqlConnection = new SqlConnection(_connString);

            try
            {
                //start timer
                var timer = Stopwatch.StartNew();

                if (!string.IsNullOrEmpty(_connString))
                {
                    (var sqlQuery, var parameterDictionary) = GenerateSqlQueryAndParameter(request.SchemaTemplate);

                    //open connection
                    _sqlConnection.Open();

                    //set SQL command
                    var command = new SqlCommand(sqlQuery, _sqlConnection);

                    //add parameters
                    if (parameterDictionary.Count > 0)
                    {
                        foreach (var param in parameterDictionary)
                        {
                            command.Parameters.AddWithValue(param.Key, param.Value);
                        }
                    }

                    //set data reader
                    var dataReader = await command.ExecuteReaderAsync(cancellationToken);

                    //load data into the data table
                    dataTable.Load(dataReader);
                }

                //update total run time
                timer.Stop();

                //set latest run values
                dataResponse.Data = dataTable;
                dataResponse.Runtime += timer.ElapsedMilliseconds;
            }
            catch (Exception ex)
            {
                dataResponse.HasErrors = true;
                Console.Write(" unable to get CG&S data. Error: {0}", ex.Message);
            }
            finally
            {
                _sqlConnection.Close();
            }

            return dataResponse;
        }

        #region Private Methods

        private (string, Dictionary<string, object>) GenerateSqlQueryAndParameter(JObject template)
        {
            //init SQL query parameter
            var parameterDictionary = new Dictionary<string, object>();
            var sqlQuery = "SELECT ";

            try
            {
                #region append data fields

                var dataFields = template.GetValue("fields");
                foreach (string field in dataFields)
                {
                    if (!field.Contains("SELECT") && !field.Contains("FROM")) //this is to check if there's no hidden select statement in the fields
                    {
                        if (invalidSQLKeys.IndexOf(field) < 0 && field != "*") //disallow those with invalidKeys and all columns searches
                        {
                            sqlQuery += $"{field},";
                        }
                    }
                }

                //drop the last , form the list this will have been added above, this quicker than
                //detecting the last item in the collection
                sqlQuery = sqlQuery[0..^1];

                #endregion append data fields

                //----------------------------------------------------------

                #region append table name

                var tableName = $"{template.GetValue("table_name")}";

                //add script-part to check if the table exist
                //this will another layer to prevent extra commands misplaced in the table_name
                var schema = string.Empty;
                var table = string.Empty;
                var checkScript = "IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE ";
                if (tableName.Contains("."))
                {
                    var tableInfo = tableName.Replace("[", "").Replace("]", "").Split('.');
                    schema = tableInfo[0];
                    table = tableInfo[1];

                    if (!string.IsNullOrEmpty(schema))
                    {
                        checkScript += $"TABLE_SCHEMA = '{schema}' ";
                    }
                    if (!string.IsNullOrEmpty(table))
                    {
                        checkScript += $"AND TABLE_NAME = '{table}'))";
                    }
                }
                else
                {
                    checkScript += $"AND TABLE_NAME = '{tableName.Replace("[", "").Replace("]", "")}'))";
                }

                //prepend checkScript to sqlQuery
                sqlQuery = checkScript + sqlQuery;

                //append table name
                sqlQuery += $" FROM {tableName}";

                #endregion append table name

                //----------------------------------------------------------

                #region append where clause

                if (template.ContainsKey("where"))
                {
                    //get where clause value
                    var whereValues = template.GetValue("where");

                    if (whereValues.Count() > 0)
                    {
                        //init where condition => true
                        sqlQuery += " WHERE 1=1 ";

                        //loop where array values
                        var loopCtr = 0;
                        foreach (var whereInfo in whereValues)
                        {
                            loopCtr++;

                            var wField = $"{whereInfo[0]}";
                            var wOperator = $"{whereInfo[1]}";
                            var wValue = $"{whereInfo[2]}";
                            var wType = $"{whereInfo[3]}";

                            if (!string.IsNullOrEmpty(wValue))
                            {
                                if (invalidSQLKeys.IndexOf(wField) < 0 && invalidSQLKeys.IndexOf(wOperator) < 0)
                                {
                                    var paramName = $"@{wField}{loopCtr}";

                                    switch (wType.ToLower())
                                    {
                                        case "integer":
                                            if (invalidSQLKeys.IndexOf(wValue) < 0)
                                            {
                                                //add to parameter dictionary
                                                if (!parameterDictionary.ContainsKey(paramName))
                                                {
                                                    parameterDictionary.Add(paramName, wValue);

                                                    //add to query
                                                    sqlQuery += $" AND {wField} {wOperator} {paramName}";
                                                }
                                            }
                                            break;

                                        case "string":
                                            //add to parameter dictionary
                                            if (!parameterDictionary.ContainsKey(paramName))
                                            {
                                                parameterDictionary.Add(paramName, wValue);

                                                //add to query
                                                sqlQuery += $" AND {wField} {wOperator} {paramName}";
                                            }

                                            break;

                                        case "date":
                                            if (wOperator.ToUpper() == "BETWEEN")
                                            {
                                                var dateInfo = wValue.Split(new string[] { "AND", "and" }, StringSplitOptions.None);
                                                var words = wValue.Split(new string[] { "AND", "and" }, StringSplitOptions.RemoveEmptyEntries);

                                                //date-from
                                                var dtFrom = false;
                                                var dateFrom = dateInfo[0].Trim().Replace("'", "");
                                                var paramDateFrom = $"@dateFrom{loopCtr}";
                                                if (!string.IsNullOrEmpty(dateFrom))
                                                {
                                                    if (IsDate(dateFrom))
                                                    {
                                                        if (!parameterDictionary.ContainsKey(paramDateFrom))
                                                        {
                                                            parameterDictionary.Add(paramDateFrom, dateFrom);
                                                            dtFrom = true;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        throw new Exception($"Unable to parse from-date ({dateFrom}) in where value {whereInfo}.");
                                                    }
                                                }

                                                //date-to
                                                var dtTo = false;
                                                var dateTo = dateInfo[1].Trim().Replace("'", "");
                                                var paramDateTo = $"@dateTo{loopCtr}";
                                                if (!string.IsNullOrEmpty(dateTo))
                                                {
                                                    if (IsDate(dateTo))
                                                    {
                                                        if (!parameterDictionary.ContainsKey(paramDateTo))
                                                        {
                                                            parameterDictionary.Add(paramDateTo, dateTo);
                                                            dtTo = true;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        throw new Exception($"Unable to parse to-date ({dateTo}) in where value {whereInfo}.");
                                                    }
                                                }

                                                if (dtFrom && dtTo)
                                                {
                                                    //add to query
                                                    sqlQuery += $" AND {wField} {wOperator} {paramDateFrom} AND {paramDateTo}";
                                                }
                                            }
                                            else
                                            {
                                                if (IsDate(wValue))
                                                {
                                                    if (!parameterDictionary.ContainsKey(paramName))
                                                    {
                                                        parameterDictionary.Add(paramName, wValue);

                                                        //add to query
                                                        sqlQuery += $" AND {wField} {wOperator} {paramName}";
                                                    }
                                                }
                                                else
                                                {
                                                    throw new Exception($"Unable to parse date ({wValue}) in where value {whereInfo}.");
                                                }
                                            }

                                            break;

                                        case "null":
                                            if (invalidSQLKeys.IndexOf(wValue) < 0)
                                            {
                                                sqlQuery += $" AND {wField} {wOperator} {wValue}";
                                            }
                                            break;
                                    }
                                }
                            }
                        }
                    }
                }

                #endregion append where clause
            }
            catch (Exception ex)
            {
                throw new Exception($"Unable to generate SQL string. Error Message: {ex.Message}");
            }

            return (sqlQuery, parameterDictionary);
        }

        private bool IsDate(string date)
        {
            try
            {
                var dt = DateTime.Parse(date);
                return true;
            }
            catch
            {
                return false;
            }
        }

        #endregion Private Methods
    }
}