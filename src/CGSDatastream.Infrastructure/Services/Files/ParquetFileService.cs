﻿using CGSDatastream.Application.Common.Models;
using CGSDatastream.Application.DataTransport.Commands.Requests;
using Humanizer;
using LCP.OCTODashboard.CGSDatastream.Application.Interfaces.Data;
using LCP.OCTODashboard.CGSDatastream.Application.Interfaces.Files;
using LCP.OCTODashboard.CGSDatastream.Application.Interfaces.Storage;
using Newtonsoft.Json.Linq;
using Parquet;
using Parquet.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LCP.OCTODashboard.CGSDatastream.Infrastructure.Services.Files
{
    public class ParquetFileService : IParquetFileService
    {
        private readonly IAwsStorageService _s3FileService;
        private readonly ICgsDataService _cgsDataService;
        private readonly IStorageService _storageService;
        private const string _completedFlag = "#Completed";
        private const string _runningFlag = "#Running";

        public ParquetFileService(IAwsStorageService s3FileService, ICgsDataService cgsDataService, IStorageService storageService)
        {
            _s3FileService = s3FileService;
            _cgsDataService = cgsDataService;
            _storageService = storageService;
        }

        public async Task<List<string>> UploadData(DatastreamUploadCommand data, CancellationToken cancellationToken)
        {
            var uploadedFiles = new List<string>();
            var schemaList = new List<string>();
            var configDataStampTemplate = new JObject();
            var totalRunTime = 0.0;
            var procCtr = 0;

            //get schema config datastamp
            if (!string.IsNullOrWhiteSpace(data.SchemaConfigDatatamp))
            {
                configDataStampTemplate = JObject.Parse(data.SchemaConfigDatatamp);

                //get recordTypes
                var schemas = $"{configDataStampTemplate["Schemas"]}";
                if (!string.IsNullOrWhiteSpace(schemas))
                {
                    var arrSchema = schemas.Split(",");
                    schemaList = new List<string>(arrSchema);
                }
            }

            foreach (var schema in data.SchemaList)
            {
                try //this try-catch block will ensure persistent processing of all schema despite having issues with some files
                {
                    //set counter
                    procCtr++;

                    //set JObject template
                    JObject schemaTemplate = JObject.Parse(schema);

                    //set filename
                    var dataLakeName = $"{schemaTemplate["DataLakeName"]}";
                    dataLakeName = dataLakeName.Trim().Replace(" ", "");

                    var fileName = $"cgs_{dataLakeName}_{DateTime.Now:yyyy-MM-dd}.parquet";

                    if (schemaList.IndexOf(dataLakeName) >= 0)
                    {
                        Console.WriteLine($"*({procCtr}) Processing DataLake: {dataLakeName}");

                        //get schema datastamp
                        var schemaDatastamp = $"{configDataStampTemplate[dataLakeName]}";

                        if (string.IsNullOrWhiteSpace(schemaDatastamp) || schemaDatastamp == _completedFlag)
                        {
                            Console.WriteLine($" => Skipped, current schema status is {schemaDatastamp}.");
                            continue;
                        }

                        //generate report file data stream
                        using var dataStream = new MemoryStream();
                        var requestModel = new DataRequestModel
                        {
                            DataLakeName = dataLakeName,
                            SchemaTemplate = schemaTemplate,
                            SchemaDatastamp = schemaDatastamp,
                            TotalRunTime = totalRunTime
                        };

                        //build report data stream
                        var buildDataResponse = await BuildReportDataStream(dataStream, requestModel, cancellationToken);

                        //start timer for the other processes as well
                        var timer = Stopwatch.StartNew();

                        //construct key name
                        var keyName = $"{dataLakeName}/{fileName}";

                        //upload data
                        if (buildDataResponse.HasData)
                        {
                            Console.Write($" => Uploading '{dataLakeName}' file ({fileName}) ...");

                            //upload file to S3 bucket
                            var uploaded = await _s3FileService.UploadFile(dataStream, keyName);

                            if (uploaded)
                            {
                                Console.WriteLine($" file is uploaded successfully.");

                                uploadedFiles.Add(keyName);
                            }
                            else
                            {
                                Console.WriteLine($" file is NOT uploaded.");
                            }
                        }
                        else
                        {
                            Console.WriteLine($" => No '{dataLakeName}' data generated/uploaded");
                        }

                        //update config datastamp
                        var schemaStatusDatastamp = buildDataResponse.LastRunHasErrors ? _runningFlag : _completedFlag;
                        Console.Write($" => Setting '{dataLakeName}' to {schemaStatusDatastamp} ...");
                        await _storageService.UpdateSchemaConfigDatastamp(dataLakeName, schemaStatusDatastamp).ConfigureAwait(false);
                        Console.WriteLine(" schema status set.");

                        //stop timer
                        timer.Stop();

                        //increment total runtime
                        totalRunTime = buildDataResponse.TotalRunTime + timer.ElapsedMilliseconds;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($" => Unexpected exception occured while processing '{schema}'.");
                    Console.WriteLine($" => Error message: {ex.Message}");
                }
            }

            //log total runtime
            var extractionTimeLapse = TimeSpan.FromMilliseconds(totalRunTime).Humanize(4);
            Console.WriteLine("--------------------------------------------------------------------------");
            Console.WriteLine($"Data extraction was completed in - {extractionTimeLapse}.");
            Console.WriteLine("--------------------------------------------------------------------------");

            return uploadedFiles;
        }

        #region Private Methods

        private async Task<BuildReportDataResponse> BuildReportDataStream(MemoryStream dataStream, DataRequestModel request, CancellationToken cancellationToken)
        {
            //int build report response
            var buildReportResponse = new BuildReportDataResponse
            {
                HasData = false,
                TotalRunTime = request.TotalRunTime
            };

            try
            {
                //set current datalake config datastamp to #Running
                Console.Write($" => Setting '{request.DataLakeName}' to {_runningFlag} ...");
                await _storageService.UpdateSchemaConfigDatastamp(request.DataLakeName, _runningFlag);
                Console.WriteLine(" schema status set.");

                //retrieve CGS data
                Console.Write($" => Retrieving data ...");
                var cgsData = await _cgsDataService.GetData(request, cancellationToken);
                Console.WriteLine($" data received after {TimeSpan.FromMilliseconds(cgsData.Runtime).Humanize(4)}.");

                //set data table
                var dataTable = cgsData.Data;

                //set response values
                buildReportResponse.TotalRunTime += cgsData.Runtime;
                buildReportResponse.HasData = dataTable.Columns.Count > 0;
                buildReportResponse.LastRunHasErrors = cgsData.HasErrors;

                if (dataTable.Rows.Count > 0)
                {
                    //generate field schema
                    var dataSchemaFields = GenerateSchema(dataTable);

                    //set size of a row group
                    var RowGroupSize = dataTable.Rows.Count + 100;

                    using (var writer = new ParquetWriter(new Schema(dataSchemaFields), dataStream))
                    {
                        //init startRow
                        var startRow = 0;

                        //keep on creating row groups until we run out of data
                        while (startRow < dataTable.Rows.Count)
                        {
                            using (var rgw = writer.CreateRowGroup())
                            {
                                // Data is written to the row group column by column
                                for (var i = 0; i < dataTable.Columns.Count; i++)
                                {
                                    //set column index
                                    var colIdx = i;

                                    //determine the target data type for the column
                                    var targetType = dataTable.Columns[colIdx].DataType;

                                    if (targetType == typeof(DateTime) || targetType == typeof(DateTimeOffset))
                                    {
                                        targetType = typeof(long);
                                    }

                                    //generate the value type, this is to ensure it can handle null values
                                    var valueType = targetType.IsClass ? targetType : typeof(Nullable<>).MakeGenericType(targetType);

                                    //create a list to hold values of the required type for the column
                                    var list = (IList)typeof(List<>)
                                                    .MakeGenericType(valueType)
                                                    .GetConstructor(Type.EmptyTypes)
                                                    .Invoke(null);

                                    //get the data to be written to the parquet stream
                                    foreach (var row in dataTable.AsEnumerable().Skip(startRow).Take(RowGroupSize))
                                    {
                                        //check if value is null, if so then add a null value
                                        if (row[colIdx] == null || row[colIdx] == DBNull.Value)
                                        {
                                            list.Add(null);
                                        }
                                        else
                                        {
                                            //init value
                                            var value = row[colIdx];

                                            var dataType = dataTable.Columns[colIdx].DataType;

                                            if (dataType == typeof(string))
                                            {
                                                var stringValue = (string)row[colIdx];

                                                value = stringValue;
                                            }
                                            else if (dataType == typeof(DateTime)) //datetime
                                            {
                                                try
                                                {
                                                    var thisDate = (DateTime)row[colIdx];
                                                    value = ((DateTimeOffset)thisDate).ToUnixTimeMilliseconds();
                                                }
                                                catch
                                                {
                                                    value = null;
                                                }
                                            }
                                            else if (dataType == typeof(System.DateTimeOffset)) //datetimeoffset
                                            {
                                                try
                                                {
                                                    var thisDate = (DateTimeOffset)row[colIdx];
                                                    value = thisDate.ToUnixTimeMilliseconds();
                                                }
                                                catch
                                                {
                                                    value = null;
                                                }
                                            }

                                            //add the value to the list,
                                            list.Add(value);
                                        }
                                    }

                                    //copy the list values to an array of the same type as the WriteColumn method expects
                                    var valuesArray = Array.CreateInstance(valueType, list.Count);
                                    list.CopyTo(valuesArray, 0);

                                    //write the column
                                    rgw.WriteColumn(new Parquet.Data.DataColumn(dataSchemaFields[i], valuesArray));
                                }
                            }

                            //increment startRow
                            startRow += RowGroupSize;
                        }
                    }
                }

                return buildReportResponse;
            }
            catch (Exception ex)
            {
                throw new Exception($"Unable to generate Parquet file. Error Message: {ex.Message}");
            }
        }

        private static List<DataField> GenerateSchema(DataTable dt)
        {
            var fields = new List<DataField>(dt.Columns.Count);

            foreach (System.Data.DataColumn column in dt.Columns)
            {
                // Attempt to parse the type of column to a parquet data type
                var success = Enum.TryParse<DataType>(column.DataType.Name, true, out var type);

                //replace DateTime / DateTimeOffset per requirement
                if (column.DataType == typeof(DateTime) || column.DataType == typeof(DateTimeOffset))
                {
                    type = DataType.Int64;
                }
                else if (!success)
                {
                    type = DataType.String;
                }

                //add column-field
                fields.Add(new DataField(column.ColumnName, type));
            }

            return fields;
        }

        #endregion Private Methods
    }
}