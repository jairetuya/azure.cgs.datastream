﻿using CGSDatastream.Application.Common.Models;
using CGSDatastream.Application.DataTransport.Commands.Requests;
using Humanizer;
using LCP.OCTODashboard.CGSDatastream.Application.Interfaces.Data;
using LCP.OCTODashboard.CGSDatastream.Application.Interfaces.Files;
using LCP.OCTODashboard.CGSDatastream.Application.Interfaces.Storage;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LCP.OCTODashboard.CGSDatastream.Infrastructure.Services.Files
{
    public class DatastreamService : IDatastreamService
    {
        private readonly IAwsStorageService _s3FileService;
        private readonly ICgsDataService _cgsDataService;
        private readonly IStorageService _storageService;
        private const string _completedFlag = "#Completed";
        private const string _runningFlag = "#Running";

        public DatastreamService(IAwsStorageService s3FileService, ICgsDataService cgsDataService, IStorageService storageService)
        {
            _s3FileService = s3FileService;
            _cgsDataService = cgsDataService;
            _storageService = storageService;
        }

        public async Task<List<string>> UploadData(DatastreamUploadCommand data, CancellationToken cancellationToken)
        {
            var uploadedFiles = new List<string>();
            var schemaList = new List<string>();
            var configDataStampTemplate = new JObject();
            var totalRunTime = 0.0;
            var procCtr = 0;

            //get schema config datastamp
            if (!string.IsNullOrWhiteSpace(data.SchemaConfigDatatamp))
            {
                configDataStampTemplate = JObject.Parse(data.SchemaConfigDatatamp);

                //get recordTypes
                var schemas = $"{configDataStampTemplate["Schemas"]}";
                if (!string.IsNullOrWhiteSpace(schemas))
                {
                    var arrSchema = schemas.Split(",");
                    schemaList = new List<string>(arrSchema);
                }
            }

            foreach (var schema in data.SchemaList)
            {
                try //this try-catch block will ensure persistent processing of all schema despite having issues with some files
                {
                    //set counter
                    procCtr++;

                    //set JObject template
                    JObject schemaTemplate = JObject.Parse(schema);

                    //set filename
                    var dataLakeName = $"{schemaTemplate["DataLakeName"]}";
                    dataLakeName = dataLakeName.Trim().Replace(" ", "");

                    var fileName = $"cgs_{dataLakeName}_{DateTime.Now:yyyy-MM-dd}.csv";

                    if (schemaList.IndexOf(dataLakeName) >= 0)
                    {
                        Console.WriteLine($"*({procCtr}) Processing DataLake: {dataLakeName}");

                        //get schema datastamp
                        var schemaDatastamp = $"{configDataStampTemplate[dataLakeName]}";

                        if (string.IsNullOrWhiteSpace(schemaDatastamp) || schemaDatastamp == _completedFlag)
                        {
                            Console.WriteLine($" => Skipped, current schema status is {schemaDatastamp}.");
                            continue;
                        }

                        //generate report file data stream
                        using var dataStream = new MemoryStream();
                        var requestModel = new DataRequestModel
                        {
                            DataLakeName = dataLakeName,
                            SchemaTemplate = schemaTemplate,
                            SchemaDatastamp = schemaDatastamp,
                            TotalRunTime = totalRunTime
                        };

                        //build report data stream
                        var buildDataResponse = await BuildReportDataStream(dataStream, requestModel, cancellationToken);

                        //start timer for the other processes as well
                        var timer = Stopwatch.StartNew();

                        //construct key name
                        var keyName = $"{dataLakeName}/{fileName}";

                        //upload data
                        if (buildDataResponse.HasData)
                        {
                            Console.Write($" => Uploading '{dataLakeName}' file ({fileName}) ...");

                            //upload file to S3 bucket
                            var uploaded = await _s3FileService.UploadFile(dataStream, keyName);

                            if (uploaded)
                            {
                                Console.WriteLine($" file is uploaded successfully.");

                                uploadedFiles.Add(keyName);
                            }
                            else
                            {
                                Console.WriteLine($" file is NOT uploaded.");
                            }
                        }
                        else
                        {
                            Console.WriteLine($" => No '{dataLakeName}' data generated/uploaded");
                        }

                        //update config datastamp
                        var schemaStatusDatastamp = buildDataResponse.LastRunHasErrors ? _runningFlag : _completedFlag;
                        Console.Write($" => Setting '{dataLakeName}' to {schemaStatusDatastamp} ...");
                        await _storageService.UpdateSchemaConfigDatastamp(dataLakeName, schemaStatusDatastamp).ConfigureAwait(false);
                        Console.WriteLine(" schema status set.");

                        //stop timer
                        timer.Stop();

                        //increment total runtime
                        totalRunTime = buildDataResponse.TotalRunTime + timer.ElapsedMilliseconds;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($" => Unexpected exception occured while processing '{schema}'.");
                    Console.WriteLine($" => Error message: {ex.Message}");
                }
            }

            //log total runtime
            var extractionTimeLapse = TimeSpan.FromMilliseconds(totalRunTime).Humanize(4);
            Console.WriteLine("--------------------------------------------------------------------------");
            Console.WriteLine($"Data extraction was completed in - {extractionTimeLapse}.");
            Console.WriteLine("--------------------------------------------------------------------------");

            return uploadedFiles;
        }

        #region Private Methods

        private async Task<BuildReportDataResponse> BuildReportDataStream(MemoryStream dataStream, DataRequestModel request, CancellationToken cancellationToken)
        {
            //int build report response
            var buildReportResponse = new BuildReportDataResponse
            {
                HasData = false,
                TotalRunTime = request.TotalRunTime
            };

            try
            {
                //set current datalake config datastamp to #Running
                Console.Write($" => Setting '{request.DataLakeName}' to {_runningFlag} ...");
                await _storageService.UpdateSchemaConfigDatastamp(request.DataLakeName, _runningFlag);
                Console.WriteLine(" schema status set.");

                //retrieve CGS data
                Console.Write($" => Retrieving data ...");
                var cgsData = await _cgsDataService.GetData(request, cancellationToken);
                Console.WriteLine($" data received after {TimeSpan.FromMilliseconds(cgsData.Runtime).Humanize(4)}.");

                //set data table
                var dataTable = cgsData.Data;

                //set response values
                buildReportResponse.TotalRunTime = cgsData.Runtime;
                buildReportResponse.HasData = dataTable.Columns.Count > 0;
                buildReportResponse.LastRunHasErrors = cgsData.HasErrors;

                if (dataTable?.Rows.Count > 0)
                {
                    //generate CSV content
                    var csvContent = GenerateCSVString(dataTable);

                    //convert data to memory stream
                    byte[] memstring = new UTF8Encoding(true).GetBytes(csvContent);
                    dataStream.Write(memstring, 0, memstring.Length);
                }

                return buildReportResponse;
            }
            catch (Exception ex)
            {
                throw new Exception($"Unable to build CSV report datastream. Error Message: {ex.Message}");
            }
        }

        private static string GenerateCSVString(DataTable dataTable, bool includeHeaderAsFirstRow = true, string separator = ",")
        {
            var csvRows = new StringBuilder("");
            var row = "";

            try
            {
                //set column count
                var columnCount = dataTable.Columns.Count;

                //create Header
                if (includeHeaderAsFirstRow)
                {
                    for (int index = 0; index < columnCount; index++)
                    {
                        row += dataTable.Columns[index];

                        if (index < columnCount - 1)
                        {
                            row += (separator);
                        }
                    }

                    row += (Environment.NewLine);
                }

                csvRows.Append(row);

                //loop rows
                for (var rowIndex = 0; rowIndex < dataTable.Rows.Count; rowIndex++)
                {
                    row = "";

                    for (var index = 0; index < columnCount; index++)
                    {
                        var value = dataTable.Rows[rowIndex][index].ToString();

                        //if type of field is string
                        if (dataTable.Rows[rowIndex][index] is string)
                        {
                            //if double quotes are used in value, ensure each are replaced by double quotes.
                            if (value.IndexOf("\"") >= 0)
                            {
                                value = value.Replace("\"", "\"\"");
                            }

                            //if separtor are is in value, ensure it is put in double quotes.
                            if (value.IndexOf(separator) >= 0)
                            {
                                value = "\"" + value + "\"";
                            }

                            //if string contain new line character
                            while (value.Contains("\r"))
                            {
                                value = value.Replace("\r", "");
                            }

                            while (value.Contains("\n"))
                            {
                                value = value.Replace("\n", "");
                            }
                        }

                        //use the field type of the header row as its always populated
                        var fieldType = dataTable.Rows[0][index].GetType();

                        if (fieldType == typeof(System.DateTimeOffset))
                        {
                            try
                            {
                                var thisDate = (DateTimeOffset)dataTable.Rows[rowIndex][index];
                                value = $"{thisDate.ToUnixTimeMilliseconds()}";
                            }
                            catch
                            {
                                value = "0";
                            }
                        }

                        if (fieldType == typeof(System.DateTime))
                        {
                            try
                            {
                                var thisDate = (DateTime)dataTable.Rows[rowIndex][index];
                                value = $"{((DateTimeOffset)thisDate).ToUnixTimeMilliseconds()}";
                            }
                            catch
                            {
                                value = "0";
                            }
                        }

                        row += value;

                        if (index < columnCount - 1)
                        {
                            row += separator;
                        }
                    }

                    dataTable.Rows[rowIndex][columnCount - 1].ToString().ToString().Replace(separator, " ");

                    row += Environment.NewLine;

                    csvRows.Append(row);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Unable to convert data into CSV-string. Error Message: {ex.Message}");
            }

            return csvRows.ToString();
        }

        #endregion Private Methods
    }
}