﻿using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using CGSDatastream.Application.Schema.Queries.Requests;
using LCP.OCTODashboard.CGSDatastream.Application.Enums;
using LCP.OCTODashboard.CGSDatastream.Application.Interfaces.Storage;
using LCP.OCTODashboard.CGSDatastream.Application.Models;
using Microsoft.WindowsAzure.Storage;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LCP.OCTODashboard.CGSDatastream.Infrastructure.Services.Storage
{
    public class AzureStorageService : IStorageService
    {
        private readonly BlobServiceClient _blobServiceClient;
        private readonly CloudStorageAccount _storageAccount;
        private readonly string _azureConnString;
        private readonly string _azureBlobName;
        private readonly string _schemaBlobContainerName;
        private readonly string _schemaDatastampFileName;
        private readonly string _resetHour;

        public AzureStorageService()
        {
            _azureConnString = Environment.GetEnvironmentVariable("AZUREWEBJOBSSTORAGE");
            _azureBlobName = Environment.GetEnvironmentVariable("AZURE_STORAGE_BLOBNAME");
            _schemaBlobContainerName = Environment.GetEnvironmentVariable("AZURE_STORAGE_SCHEMA_BLOBNAME");
            _schemaDatastampFileName = Environment.GetEnvironmentVariable("AZURE_STORAGE_SCHEMA_FILENAME");
            _resetHour = Environment.GetEnvironmentVariable("RESET_HOUR");

            //set new blob service client
            _blobServiceClient = new BlobServiceClient(_azureConnString);

            //setup the connection to the storage account
            _storageAccount = CloudStorageAccount.Parse(_azureConnString);
        }

        public async Task<(List<string>, string)> GetSchemaContents(GetSchemaQuery request)
        {
            var schemaList = new List<string>();

            //get blob Items
            var blobItems = GetBlobItems(_azureBlobName);

            foreach (var blobItem in blobItems)
            {
                //only application/json blob items
                if (blobItem.Properties.ContentType == "application/json")
                {
                    //set storage data
                    var storageData = new AzureStorageData
                    {
                        BlobName = _azureBlobName,
                        FileName = blobItem.Name
                    };

                    //get blob content
                    var schema = await GetBlobContent(storageData).ConfigureAwait(false);

                    if (!string.IsNullOrEmpty(schema))
                    {
                        if (IsValidSchema(schema))
                        {
                            schemaList.Add(schema);
                        }
                    }
                }
            }

            //set schema config datastamps
            var schemaConfigDatastamp = await SetSchemaConfigurations(schemaList, request.RunMode);

            return (schemaList, schemaConfigDatastamp);
        }

        public async Task<string> UpdateSchemaConfigDatastamp(string schemaName, string schemaDatastamp)
        {
            try
            {
                var updateStatus = "Datastamp is empty.";

                if (!string.IsNullOrWhiteSpace(schemaDatastamp))
                {
                    //set storage data
                    var storageData = new AzureStorageData
                    {
                        BlobName = _schemaBlobContainerName,
                        FileName = _schemaDatastampFileName
                    };

                    //retrieve config datastamp
                    var schemaConfigDataStamp = await GetBlobContent(storageData);

                    //set JObject template
                    JObject configDataStampTemplate = JObject.Parse(schemaConfigDataStamp);

                    //get schemaList
                    var schemaList = new List<string>();
                    var schemas = $"{configDataStampTemplate["Schemas"]}";
                    if (!string.IsNullOrWhiteSpace(schemas))
                    {
                        var arrSchema = schemas.Split(",");
                        schemaList = new List<string>(arrSchema);
                    }

                    //compose schema config value
                    var updatedSchemaDatastamp = new StringBuilder();
                    updatedSchemaDatastamp.Append("{");
                    updatedSchemaDatastamp.Append($"\"Schemas\":\"{string.Join(",", schemaList)}\",");

                    foreach (var s in schemaList)
                    {
                        var datastamp = s == schemaName ? schemaDatastamp : $"{configDataStampTemplate[s]}";
                        updatedSchemaDatastamp.Append($"\"{s}\":\"{datastamp}\",");
                    }

                    updatedSchemaDatastamp.Append("}");

                    //-------------------------------------------------------------------------
                    //update schema config datastamp
                    await UpdateBlobStorageData(storageData, updatedSchemaDatastamp.ToString());

                    updateStatus = "Updated";
                }

                return updateStatus;
            }
            catch (Exception ex)
            {
                var excMsg = $"Unable to update schema config datastamp ({_schemaDatastampFileName}). Error: {ex.Message}";
                Console.WriteLine(excMsg);
                return excMsg;
            }
        }

        #region Private Methods

        private Azure.Pageable<BlobItem> GetBlobItems(string blobName)
        {
            try
            {
                //create the container and return a container client object
                var containerClient = _blobServiceClient.GetBlobContainerClient(blobName);

                return containerClient.GetBlobs();
            }
            catch (Exception ex)
            {
                throw new Exception($"Unable to get blob items. Error Message: {ex.Message}");
            }
        }

        private async Task<string> GetBlobContent(AzureStorageData data)
        {
            string blobContent;

            try
            {
                //connect to the blob storage
                var serviceClient = _storageAccount.CreateCloudBlobClient();

                //connect to the blob container
                var container = serviceClient.GetContainerReference(data.BlobName);

                //connect to the blob file
                var blob = container.GetBlockBlobReference($"{data.FileName}");

                //return the blob file as text
                blobContent = await blob.DownloadTextAsync().ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                throw new Exception($"Unable to get blob content ({data.FileName}). Error Message: {ex.Message}");
            }

            return blobContent;
        }

        private static bool IsValidSchema(string schema)
        {
            //set JObject template
            JObject template = JObject.Parse(schema);

            //check required template fields; skip if not found
            if (!template.ContainsKey("DataLakeName") || !template.ContainsKey("table_name") || !template.ContainsKey("fields"))
            {
                return false;
            }

            return true;
        }

        private async Task<string> SetSchemaConfigurations(List<string> schemaList, RunMode runMode)
        {
            //set schemas container
            var schemas = new List<string>();
            const string _notStartedFlag = "#NotStarted";

            //loop through schema list
            foreach (var schema in schemaList)
            {
                //set JObject template
                JObject schemaTemplate = JObject.Parse(schema);

                //get datalake name
                var dataLakeName = $"{schemaTemplate["DataLakeName"]}";

                if (schemas.IndexOf(dataLakeName) < 0) //this is ensure no similar datalakename is set in a separate schema file
                {
                    schemas.Add(dataLakeName.Trim().Replace(" ", ""));
                }
            }

            //compose schema config value
            var schemaConfigs = new StringBuilder();
            schemaConfigs.Append("{");
            schemaConfigs.Append($"\"Schemas\":\"{string.Join(",", schemas)}\",");

            foreach (var s in schemas)
            {
                schemaConfigs.Append($"\"{s}\":\"{_notStartedFlag}\",");
            }

            schemaConfigs.Append("}");

            //init config datastamp value
            var configDatastamp = schemaConfigs.ToString();

            //set storage data
            var storageData = new AzureStorageData
            {
                BlobName = _schemaBlobContainerName,
                FileName = _schemaDatastampFileName
            };

            var createSchemaStorage = false;

            try
            {
                if (DateTime.Now.Hour.ToString() == _resetHour || runMode == RunMode.BlobTrigger)
                {
                    await UpdateBlobStorageData(storageData, configDatastamp);
                }
                else
                {
                    configDatastamp = await GetBlobContent(storageData);
                }
            }
            catch (Exception ex)
            {
                createSchemaStorage = true;

                Console.WriteLine($" => Unable to update schema configuration ({_schemaDatastampFileName}). Error: {ex.Message}");
            }

            if (createSchemaStorage)
            {
                //create blob content
                await UpdateBlobStorageData(storageData, configDatastamp);

                Console.WriteLine($" => Schema blob container ({_schemaBlobContainerName}) was created successfully.");
            }

            return configDatastamp;
        }

        private async Task UpdateBlobStorageData(AzureStorageData data, string storageData)
        {
            //create container
            var containerCreated = await CreateBlobContainer(data.BlobName);

            if (containerCreated)
            {
                //connect to the blob storage
                var serviceClient = _storageAccount.CreateCloudBlobClient();

                //connect to the blob container
                var container = serviceClient.GetContainerReference(data.BlobName);

                //connect to the blob file
                var blob = container.GetBlockBlobReference(data.FileName);

                //updated storage data
                await blob.UploadTextAsync(storageData);
            }
        }

        private async Task<bool> CreateBlobContainer(string containerName)
        {
            var containerCreated = false;
            try
            {
                // Create the container
                BlobContainerClient container = await _blobServiceClient.CreateBlobContainerAsync(containerName);

                if (await container.ExistsAsync())
                {
                    Console.WriteLine("Created container {0}", container.Name);
                    containerCreated = true;
                }
            }
            catch
            {
                //when this happens if the container is already created
                containerCreated = true;
            }

            return containerCreated;
        }

        #endregion Private Methods
    }
}