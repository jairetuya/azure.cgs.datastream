﻿using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Transfer;
using LCP.OCTODashboard.CGSDatastream.Application.Interfaces.Storage;
using System;
using System.IO;
using System.Threading.Tasks;

namespace LCP.OCTODashboard.CGSDatastream.Infrastructure.Services.Storage
{
    public class AwsStorageService : IAwsStorageService
    {
        public readonly AmazonS3Client _s3Client;
        private readonly string _accessId;
        private readonly string _accessKey;
        private readonly string _bucketName;

        public AwsStorageService()
        {
            _accessId = Environment.GetEnvironmentVariable("AWS_ACCESS_ID");
            _accessKey = Environment.GetEnvironmentVariable("AWS_ACCESS_KEY");
            _bucketName = Environment.GetEnvironmentVariable("AWS_BUCKET_NAME");

            //init S3 client
            var awsCredentials = new BasicAWSCredentials(_accessId, _accessKey);
            _s3Client = new AmazonS3Client(awsCredentials, RegionEndpoint.USEast1);
        }

        public async Task<bool> UploadFile(MemoryStream memoryStream, string keyName)
        {
            try
            {
                var transferUtility = new TransferUtility(_s3Client);
                await transferUtility.UploadAsync(memoryStream, _bucketName, keyName);

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception($"Error uploading stream file ({keyName}) to S3 bucket. Error: {ex.Message}");
            }
        }
    }
}