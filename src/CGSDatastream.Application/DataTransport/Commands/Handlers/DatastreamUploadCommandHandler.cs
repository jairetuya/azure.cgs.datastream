﻿using CGSDatastream.Application.DataTransport.Commands.Requests;
using LCP.OCTODashboard.CGSDatastream.Application.Enums;
using LCP.OCTODashboard.CGSDatastream.Application.Interfaces.Files;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace CGSDatastream.Application.DataTransport.Commands.Handlers
{
    public class DatastreamUploadCommandHandler : IRequestHandler<DatastreamUploadCommand, List<string>>
    {
        private readonly IParquetFileService _parquetFileService;
        private readonly IDatastreamService _datastreamService;

        public DatastreamUploadCommandHandler(IParquetFileService parquetFileService, IDatastreamService datastreamService)
        {
            _parquetFileService = parquetFileService;
            _datastreamService = datastreamService;
        }

        public async Task<List<string>> Handle(DatastreamUploadCommand command, CancellationToken cancellationToken)
        {
            var uploadedFiles = new List<string>();

            if (string.Equals(command.PreferredFileType, nameof(FileType.Parquet), StringComparison.OrdinalIgnoreCase))
            {
                uploadedFiles = await _parquetFileService.UploadData(command, cancellationToken);
            }
            else if (string.Equals(command.PreferredFileType, nameof(FileType.CSV), StringComparison.OrdinalIgnoreCase))
            {
                uploadedFiles = await _datastreamService.UploadData(command, cancellationToken);
            }

            return uploadedFiles;
        }
    }
}