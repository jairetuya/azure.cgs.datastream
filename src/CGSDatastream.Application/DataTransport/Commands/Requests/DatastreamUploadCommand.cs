﻿using LCP.OCTODashboard.CGSDatastream.Application.Enums;
using MediatR;
using System.Collections.Generic;

namespace CGSDatastream.Application.DataTransport.Commands.Requests
{
    public class DatastreamUploadCommand : IRequest<List<string>>
    {
        public List<string> SchemaList { get; set; }

        public string PreferredFileType { get; set; }

        public string SchemaConfigDatatamp { get; set; }

        public RunMode RunMode { get; set; }
    }
}