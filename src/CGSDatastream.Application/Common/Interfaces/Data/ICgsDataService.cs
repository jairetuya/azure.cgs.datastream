﻿using CGSDatastream.Application.Common.Models;
using System.Threading;
using System.Threading.Tasks;

namespace LCP.OCTODashboard.CGSDatastream.Application.Interfaces.Data
{
    public interface ICgsDataService
    {
        Task<DataResponseModel> GetData(DataRequestModel request, CancellationToken cancellationToken);
    }
}