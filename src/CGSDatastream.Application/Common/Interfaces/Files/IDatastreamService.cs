﻿using CGSDatastream.Application.DataTransport.Commands.Requests;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace LCP.OCTODashboard.CGSDatastream.Application.Interfaces.Files
{
    public interface IDatastreamService
    {
        Task<List<string>> UploadData(DatastreamUploadCommand data, CancellationToken cancellationToken);
    }
}