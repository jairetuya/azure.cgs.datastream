﻿using System.IO;
using System.Threading.Tasks;

namespace LCP.OCTODashboard.CGSDatastream.Application.Interfaces.Storage
{
    public interface IAwsStorageService
    {
        Task<bool> UploadFile(MemoryStream memoryStream, string folderName);
    }
}