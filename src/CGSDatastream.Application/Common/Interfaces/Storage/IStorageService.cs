﻿using CGSDatastream.Application.Schema.Queries.Requests;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LCP.OCTODashboard.CGSDatastream.Application.Interfaces.Storage
{
    public interface IStorageService
    {
        Task<(List<string>, string)> GetSchemaContents(GetSchemaQuery request);

        Task<string> UpdateSchemaConfigDatastamp(string schemaName, string schemaDatastamp);
    }
}