﻿using System.Data;

namespace CGSDatastream.Application.Common.Models
{
    public class DataResponseModel
    {
        public DataTable Data { get; set; }

        public double Runtime { get; set; }

        public bool HasErrors { get; set; } = false;
    }
}