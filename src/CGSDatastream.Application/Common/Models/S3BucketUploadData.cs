﻿namespace LCP.OCTODashboard.CGSDatastream.Application.Models
{
    public class S3BucketUploadData
    {
        public string OutputFile { get; set; }

        public string FileName { get; set; }

        public string KeyName { get; set; }

        public string Data { get; set; }
    }
}