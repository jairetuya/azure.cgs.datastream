﻿using Newtonsoft.Json.Linq;

namespace CGSDatastream.Application.Common.Models
{
    public class DataRequestModel
    {
        public string DataLakeName { get; set; }

        public JObject SchemaTemplate { get; set; }

        public string SchemaDatastamp { get; set; }

        public double TotalRunTime { get; set; }
    }
}