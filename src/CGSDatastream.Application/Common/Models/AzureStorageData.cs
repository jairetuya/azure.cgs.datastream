﻿namespace LCP.OCTODashboard.CGSDatastream.Application.Models
{
    public class AzureStorageData
    {
        public string ConnectionString { get; set; }

        public string BlobName { get; set; }

        public string FileName { get; set; }
    }
}