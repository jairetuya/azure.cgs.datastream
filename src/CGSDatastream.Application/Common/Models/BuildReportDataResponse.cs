﻿namespace CGSDatastream.Application.Common.Models
{
    public class BuildReportDataResponse
    {
        public bool HasData { get; set; }

        public double TotalRunTime { get; set; }

        public bool LastRunHasErrors { get; set; } = false;
    }
}