﻿using CGSDatastream.Application.Schema.Queries.Requests;
using LCP.OCTODashboard.CGSDatastream.Application.Interfaces.Storage;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace CGSDatastream.Application.Schema.Queries.Handlers
{
    public class GetSchemaQueryHandler : IRequestHandler<GetSchemaQuery, (List<string>, string)>
    {
        private readonly IStorageService _storageService;

        public GetSchemaQueryHandler(IStorageService storageService)
        {
            _storageService = storageService;
        }

        public async Task<(List<string>, string)> Handle(GetSchemaQuery request, CancellationToken cancellationToken)
        {
            return await _storageService.GetSchemaContents(request);
        }
    }
}