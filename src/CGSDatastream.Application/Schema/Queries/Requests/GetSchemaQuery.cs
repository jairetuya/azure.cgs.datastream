﻿using LCP.OCTODashboard.CGSDatastream.Application.Enums;
using MediatR;
using System.Collections.Generic;

namespace CGSDatastream.Application.Schema.Queries.Requests
{
    public class GetSchemaQuery : IRequest<(List<string>, string)>
    {
        public RunMode RunMode { get; set; }
    }
}