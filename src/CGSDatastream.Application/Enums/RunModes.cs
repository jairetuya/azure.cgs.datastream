﻿namespace LCP.OCTODashboard.CGSDatastream.Application.Enums
{
    public enum RunMode
    {
        TimerTrigger,
        BlobTrigger
    }
}