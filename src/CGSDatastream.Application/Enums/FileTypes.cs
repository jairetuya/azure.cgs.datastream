﻿namespace LCP.OCTODashboard.CGSDatastream.Application.Enums
{
    public enum FileType
    {
        Parquet,
        CSV
    }
}