﻿using CGSDatastream.Application.DataTransport.Commands.Requests;
using CGSDatastream.Application.Schema.Queries.Requests;
using LCP.OCTODashboard.CGSDatastream.Application.Enums;
using MediatR;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace LCP.OCTODashboard.CGSDatastream.Functions
{
    public class CGSDatastreamBlobTriggerFunction
    {
        #region Private Fields

        private readonly IServiceProvider _serviceProvider;
        private const string _functionName = "CG&S Datastream [blob trigger]";

        #endregion Private Fields

        #region Constructor

        public CGSDatastreamBlobTriggerFunction(IServiceProvider serviceProvider, string caller = null)
        {
            switch (caller)
            {
                case "Testing": _serviceProvider = serviceProvider; break;
                default: _serviceProvider = Startup.BuildContainer().BuildServiceProvider(); break;
            }
        }

        #endregion Constructor

        [FunctionName("Extract_Pull_BlobTrigger")]
        public async Task Run([BlobTrigger("%AZURE_STORAGE_BLOBNAME%/{name}", Connection = "AZUREWEBJOBSSTORAGE")] Stream cgsBlob, ILogger log)
        {
            //log execution time
            log.LogInformation($"** {_functionName} function executed at: {DateTime.Now}");

            //init mediator service provider
            var mediator = _serviceProvider.GetService<IMediator>();

            //set run-mode: blob-trigger
            var runMode = RunMode.BlobTrigger;

            //get schema list
            log.LogInformation("* Retrieving schema files and datastamps ... ");
            (var schemaList, var schemaConfigDatastamp) = await mediator.Send(new GetSchemaQuery { RunMode = runMode });

            //set generated files container
            var generatedFiles = new List<string>();

            //upload schema-template data
            if (schemaList.Count > 0)
            {
                //log number of schema files to process
                log.LogInformation($"* There are {schemaList.Count} schema files to process.");

                var preferredFileType = Environment.GetEnvironmentVariable("PREFERRED_FILE_TYPE");
                log.LogInformation($"* Preferred File Type: {preferredFileType}");

                //commence data pull command
                generatedFiles = await mediator.Send(new DatastreamUploadCommand
                {
                    SchemaList = schemaList,
                    PreferredFileType = preferredFileType,
                    SchemaConfigDatatamp = schemaConfigDatastamp,
                    RunMode = runMode
                });
            }

            //display generated files
            if (generatedFiles.Count > 0)
            {
                log.LogInformation($"** The following ({generatedFiles.Count}) files are uploaded successfully:");
                foreach (var file in generatedFiles)
                {
                    log.LogInformation(file);
                }
            }
            else
            {
                log.LogInformation("** No CGS datastream files uploaded.");
            }
        }
    }
}