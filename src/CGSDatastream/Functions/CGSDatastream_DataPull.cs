﻿using CGSDatastream.Application.DataTransport.Commands.Requests;
using CGSDatastream.Application.Schema.Queries.Requests;
using LCP.OCTODashboard.CGSDatastream.Application.Enums;
using MediatR;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LCP.OCTODashboard.CGSDatastream.Functions
{
    public class CGSDatastreamFunction
    {
        #region Private Fields

        private readonly IServiceProvider _serviceProvider;
        private const string _functionName = "CG&S Datastream [timer trigger]";

        #endregion Private Fields

        #region Constructor

        public CGSDatastreamFunction(IServiceProvider serviceProvider, string caller = null)
        {
            switch (caller)
            {
                case "Testing": _serviceProvider = serviceProvider; break;
                default: _serviceProvider = Startup.BuildContainer().BuildServiceProvider(); break;
            }
        }

        #endregion Constructor

        [FunctionName("Extract_Pull")]
        public async Task Run([TimerTrigger("%RUN_SCHEDULE%")] TimerInfo CGSDatastreamTimer, ILogger log, ExecutionContext context)
        {
            //log execution time
            log.LogInformation($"** {_functionName} function executed at: {DateTime.Now}");

            //log if timer is past due
            if (CGSDatastreamTimer.IsPastDue)
            {
                log.LogInformation($"** {_functionName} function is running late!");
            }

            //init mediator service provider
            var mediator = _serviceProvider.GetService<IMediator>();

            //set run-mode
            var runMode = RunMode.TimerTrigger;

            //get schema list
            log.LogInformation("* Retrieving schema files and datastamps ... ");
            (var schemaList, var schemaConfigDatastamp) = await mediator.Send(new GetSchemaQuery { RunMode = runMode });

            //set generated files container
            var generatedFiles = new List<string>();

            //upload schema-template data
            if (schemaList.Count > 0)
            {
                //log number of schema files to process
                log.LogInformation($"* There are {schemaList.Count} schema files to process.");

                var preferredFileType = Environment.GetEnvironmentVariable("PREFERRED_FILE_TYPE");
                log.LogInformation($"* Preferred File Type: {preferredFileType}");

                //commence data pull command
                generatedFiles = await mediator.Send(new DatastreamUploadCommand
                {
                    SchemaList = schemaList,
                    PreferredFileType = preferredFileType,
                    SchemaConfigDatatamp = schemaConfigDatastamp,
                    RunMode = runMode
                });
            }

            //display generated files
            if (generatedFiles.Count > 0)
            {
                log.LogInformation($"** The following ({generatedFiles.Count}) files are uploaded successfully:");
                foreach (var file in generatedFiles)
                {
                    log.LogInformation(file);
                }
            }
            else
            {
                log.LogInformation("** No CGS datastream files uploaded.");
            }
        }
    }
}