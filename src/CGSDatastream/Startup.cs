﻿using LCP.OCTODashboard.CGSDatastream.Application;
using LCP.OCTODashboard.CGSDatastream.Infrastructure;
using Microsoft.Extensions.DependencyInjection;

namespace LCP.OCTODashboard.CGSDatastream
{
    public class Startup
    {
        public static IServiceCollection BuildContainer()
        {
            var services = new ServiceCollection();
            services
                .AddApplication()
                .AddInfrastructure();

            return services;
        }
    }
}