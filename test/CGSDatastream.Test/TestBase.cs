using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Spackle;
using System.Collections.Generic;
using System.Text;

namespace CGSDatastream.Test
{
    public abstract class TestBase
    {
        protected RandomObjectGenerator RandomGenerator { get; }
        private readonly List<Moq.Mock> _mocks;

        protected TestBase()
        {
            RandomGenerator = new RandomObjectGenerator();
            _mocks = new List<Moq.Mock>();
        }

        protected Mock<T> CreateMock<T>()
            where T : class
        {
            var ret = new Mock<T>(MockBehavior.Strict);

            _mocks.Add(ret);

            return ret;
        }

        [TestCleanup]
        public void BaseCleanup()
        {
            foreach (var mock in _mocks)
            {
                mock.VerifyAll();
            }
        }

        protected SchemaResponse GenerateSchemaTemplateList(int templateCount, string dataLakeName, bool validTemplate = false, string status = "#NotStarted")
        {
            var schemaTemplateList = new List<string>();
            var schemaList = new List<string>();

            for (var c = 0; c < templateCount; c++)
            {
                var rndDataLakeName = validTemplate && templateCount == 1 ? dataLakeName : RandomGenerator.Generate<string>();
                var schemaTemplate = "{" + $"\"DataLakeName\": \"{rndDataLakeName}\",\n    \"table_name\":\"[MiCloudServiceAPI].[Asset_ClassificationUsage]\",\n \"fields\":\n\t[\n\t\t\"id\",\n\t\t\"Type\",\n\t\t\"ClassificationName\",\n\t\t\"ClassificationId\",\n\t\t\"AssetId\",\n\t\t\"createdat\",\n\t\t\"updatedat\",\n\t\t\"deleted\"\n\t],\n\"where\":\n\t[\n\t\t[\"Deleted\", \"!=\", \"1\", \"integer\"]\n\t]" + "}";
                if (!validTemplate)
                {
                    schemaTemplate = RandomGenerator.Generate<string>();
                }

                //add to schemas
                schemaList.Add(rndDataLakeName);

                //add to schema templates
                schemaTemplateList.Add(schemaTemplate);
            }

            return new SchemaResponse
            {
                SchemaList = schemaList,
                SchemaTemplateList = schemaTemplateList,
                SchemaConfigDatastamp = GenerateSchemaConfigDatastamps(schemaList, status)
            };
        }

        protected string GenerateSchemaConfigDatastamps(List<string> schemaList, string status)
        {
            //compose schema config value
            var schemaConfigs = new StringBuilder();
            schemaConfigs.Append("{");
            schemaConfigs.Append($"\"Schemas\":\"{string.Join(",", schemaList)}\",");

            foreach (var s in schemaList)
            {
                schemaConfigs.Append($"\"{s}\":\"{status}\",");
            }

            schemaConfigs.Append("}");

            //init config datastamp value
            return schemaConfigs.ToString();
        }

        protected class SchemaResponse
        {
            public List<string> SchemaTemplateList { get; set; }

            public string SchemaConfigDatastamp { get; set; }

            public List<string> SchemaList { get; set; }
        }
    }
}