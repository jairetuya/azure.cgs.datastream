﻿using CGSDatastream.Application.Common.Models;
using CGSDatastream.Application.DataTransport.Commands.Requests;
using LCP.OCTODashboard.CGSDatastream.Application.Enums;
using LCP.OCTODashboard.CGSDatastream.Application.Interfaces.Data;
using LCP.OCTODashboard.CGSDatastream.Application.Interfaces.Storage;
using LCP.OCTODashboard.CGSDatastream.Infrastructure.Services.Files;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace CGSDatastream.Test.Infrastructure
{
    [TestClass]
    public class DatastreamServiceTest : TestBase
    {
        private DatastreamService _sut;
        private readonly Mock<IAwsStorageService> _mockS3FileService;
        private readonly Mock<IStorageService> _mockStorageService;
        private readonly Mock<ICgsDataService> _cgsDataService;

        public DatastreamServiceTest()
        {
            _mockS3FileService = CreateMock<IAwsStorageService>();
            _mockStorageService = CreateMock<IStorageService>();
            _cgsDataService = CreateMock<ICgsDataService>();
        }

        [TestInitialize]
        public void Init()
        {
        }

        [TestMethod]
        public async Task UploadData_OneSchema_NotStarted_Test()
        {
            // Arrange
            var templateCount = 1;
            var dataLakeName = RandomGenerator.Generate<string>();
            var validTemplate = true;

            //setup report file service: get schema list
            var schemaData = GenerateSchemaTemplateList(templateCount, dataLakeName, validTemplate, "#NotStarted");

            //set upload data command
            var uploadData = new DatastreamUploadCommand
            {
                SchemaList = schemaData.SchemaTemplateList,
                SchemaConfigDatatamp = schemaData.SchemaConfigDatastamp,
                RunMode = RunMode.TimerTrigger
            };

            //mock report file service: UpdateDatastamp
            _mockStorageService
                .Setup(_ => _.UpdateSchemaConfigDatastamp(
                    It.Is<string>(_ => _ == dataLakeName),
                    It.IsAny<string>()))
                .ReturnsAsync("OK")
                .Verifiable();

            //mock SysAid service: GetData
            var dataResponse = new DataResponseModel
            {
                Data = GenerateDataTable(),
                HasErrors = false,
                Runtime = RandomGenerator.Generate<double>()
            };

            _cgsDataService
                .Setup(_ => _.GetData(
                    It.IsAny<DataRequestModel>(),
                    It.IsAny<CancellationToken>()))
                .ReturnsAsync(dataResponse)
                .Verifiable();

            _mockS3FileService
                .Setup(_ => _.UploadFile(
                        It.IsAny<MemoryStream>(),
                        It.IsAny<string>()
                    ))
                .ReturnsAsync(true)
                .Verifiable();

            // Act
            _sut = new DatastreamService(_mockS3FileService.Object, _cgsDataService.Object, _mockStorageService.Object);
            var result = await _sut.UploadData(uploadData, default);

            // Assert
            Assert.IsInstanceOfType(result, typeof(List<string>));
            Assert.AreEqual(result.Count, templateCount);
            Assert.AreEqual(result[0], $"{dataLakeName}/cgs_{dataLakeName}_{DateTime.Now:yyyy-MM-dd}.csv");

            _cgsDataService.Verify(_ =>
                _.GetData(
                    It.IsAny<DataRequestModel>(),
                    It.IsAny<CancellationToken>()),
                    Times.Exactly(templateCount));

            _mockS3FileService.Verify(_ =>
                _.UploadFile(
                    It.IsAny<MemoryStream>(),
                    It.IsAny<string>()),
                    Times.Exactly(templateCount));

            _mockStorageService.Verify(_ =>
                _.UpdateSchemaConfigDatastamp(
                    It.IsAny<string>(),
                    It.IsAny<string>()),
                    Times.Exactly(templateCount * 2));
        }

        [TestMethod]
        public async Task UploadData_OneSchema_Running_Test()
        {
            // Arrange
            var templateCount = 1;
            var dataLakeName = RandomGenerator.Generate<string>();
            var validTemplate = true;

            //setup report file service: get schema list
            var schemaData = GenerateSchemaTemplateList(templateCount, dataLakeName, validTemplate, "#Running");

            //set upload data command
            var uploadData = new DatastreamUploadCommand
            {
                SchemaList = schemaData.SchemaTemplateList,
                SchemaConfigDatatamp = schemaData.SchemaConfigDatastamp,
                RunMode = RunMode.TimerTrigger
            };

            //mock report file service: UpdateDatastamp
            _mockStorageService
                .Setup(_ => _.UpdateSchemaConfigDatastamp(
                    It.Is<string>(_ => _ == dataLakeName),
                    It.IsAny<string>()))
                .ReturnsAsync("OK")
                .Verifiable();

            //mock SysAid service: GetData
            var dataResponse = new DataResponseModel
            {
                Data = GenerateDataTable(),
                HasErrors = false,
                Runtime = RandomGenerator.Generate<double>()
            };

            _cgsDataService
                .Setup(_ => _.GetData(
                    It.IsAny<DataRequestModel>(),
                    It.IsAny<CancellationToken>()))
                .ReturnsAsync(dataResponse)
                .Verifiable();

            _mockS3FileService
                .Setup(_ => _.UploadFile(
                        It.IsAny<MemoryStream>(),
                        It.IsAny<string>()
                    ))
                .ReturnsAsync(true)
                .Verifiable();

            // Act
            _sut = new DatastreamService(_mockS3FileService.Object, _cgsDataService.Object, _mockStorageService.Object);
            var result = await _sut.UploadData(uploadData, default);

            // Assert
            Assert.IsInstanceOfType(result, typeof(List<string>));
            Assert.AreEqual(result.Count, templateCount);
            Assert.AreEqual(result[0], $"{dataLakeName}/cgs_{dataLakeName}_{DateTime.Now:yyyy-MM-dd}.csv");

            _cgsDataService.Verify(_ =>
                _.GetData(
                    It.IsAny<DataRequestModel>(),
                    It.IsAny<CancellationToken>()),
                    Times.Exactly(templateCount));

            _mockS3FileService.Verify(_ =>
                _.UploadFile(
                    It.IsAny<MemoryStream>(),
                    It.IsAny<string>()),
                    Times.Exactly(templateCount));

            _mockStorageService.Verify(_ =>
                _.UpdateSchemaConfigDatastamp(
                    It.IsAny<string>(),
                    It.IsAny<string>()),
                    Times.Exactly(templateCount * 2));
        }

        [TestMethod]
        public async Task UploadData_OneSchema_Completed_Test()
        {
            // Arrange
            var templateCount = 1;
            var dataLakeName = RandomGenerator.Generate<string>();
            var validTemplate = true;

            //setup report file service: get schema list
            var schemaData = GenerateSchemaTemplateList(templateCount, dataLakeName, validTemplate, "#Completed");

            //set upload data command
            var uploadData = new DatastreamUploadCommand
            {
                SchemaList = schemaData.SchemaTemplateList,
                SchemaConfigDatatamp = schemaData.SchemaConfigDatastamp,
                RunMode = RunMode.TimerTrigger
            };

            // Act
            _sut = new DatastreamService(_mockS3FileService.Object, _cgsDataService.Object, _mockStorageService.Object);
            var result = await _sut.UploadData(uploadData, default);

            // Assert
            Assert.IsInstanceOfType(result, typeof(List<string>));
            Assert.AreEqual(result.Count, 0);
        }

        [TestMethod]
        public async Task UploadData_MultipleSchema_NotStarted_Test()
        {
            // Arrange
            var templateCount = 100;
            var validTemplate = true;

            //setup report file service: get schema list
            var schemaData = GenerateSchemaTemplateList(templateCount, "", validTemplate, "#NotStarted");

            //set upload data command
            var uploadData = new DatastreamUploadCommand
            {
                SchemaList = schemaData.SchemaTemplateList,
                SchemaConfigDatatamp = schemaData.SchemaConfigDatastamp,
                RunMode = RunMode.TimerTrigger
            };

            //mock report file service: UpdateDatastamp
            _mockStorageService
                .Setup(_ => _.UpdateSchemaConfigDatastamp(
                    It.IsAny<string>(),
                    It.IsAny<string>()))
                .ReturnsAsync("OK")
                .Verifiable();

            //mock SysAid service: GetData
            var dataResponse = new DataResponseModel
            {
                Data = GenerateDataTable(),
                HasErrors = false,
                Runtime = RandomGenerator.Generate<double>()
            };

            _cgsDataService
                .Setup(_ => _.GetData(
                    It.IsAny<DataRequestModel>(),
                    It.IsAny<CancellationToken>()))
                .ReturnsAsync(dataResponse)
                .Verifiable();

            _mockS3FileService
                .Setup(_ => _.UploadFile(
                        It.IsAny<MemoryStream>(),
                        It.IsAny<string>()
                    ))
                .ReturnsAsync(true)
                .Verifiable();

            // Act
            _sut = new DatastreamService(_mockS3FileService.Object, _cgsDataService.Object, _mockStorageService.Object);
            var result = await _sut.UploadData(uploadData, default);

            // Assert
            Assert.IsInstanceOfType(result, typeof(List<string>));
            Assert.AreEqual(result.Count, templateCount);

            for (var x = 0; x <= templateCount - 1; x++)
            {
                var generatedFile = $"{schemaData.SchemaList[x]}/cgs_{schemaData.SchemaList[x]}_{DateTime.Now:yyyy-MM-dd}.csv";

                //should exist in the return result
                Assert.IsTrue(result.IndexOf(generatedFile) >= 0);
            }

            _cgsDataService.Verify(_ =>
                _.GetData(
                    It.IsAny<DataRequestModel>(),
                    It.IsAny<CancellationToken>()),
                    Times.Exactly(templateCount));

            _mockS3FileService.Verify(_ =>
                _.UploadFile(
                    It.IsAny<MemoryStream>(),
                    It.IsAny<string>()),
                    Times.Exactly(templateCount));

            _mockStorageService.Verify(_ =>
                _.UpdateSchemaConfigDatastamp(
                    It.IsAny<string>(),
                    It.IsAny<string>()),
                    Times.Exactly(templateCount * 2));
        }

        [TestMethod]
        public async Task UploadData_MultipleSchema_Completed_Test()
        {
            // Arrange
            var templateCount = 100;
            var validTemplate = true;

            //setup report file service: get schema list
            var schemaData = GenerateSchemaTemplateList(templateCount, "", validTemplate, "#Completed");

            //set upload data command
            var uploadData = new DatastreamUploadCommand
            {
                SchemaList = schemaData.SchemaTemplateList,
                SchemaConfigDatatamp = schemaData.SchemaConfigDatastamp,
                RunMode = RunMode.TimerTrigger
            };

            // Act
            _sut = new DatastreamService(_mockS3FileService.Object, _cgsDataService.Object, _mockStorageService.Object);
            var result = await _sut.UploadData(uploadData, default);

            // Assert
            Assert.IsInstanceOfType(result, typeof(List<string>));
            Assert.AreEqual(result.Count, 0);
        }

        [TestMethod]
        public async Task UploadData_OneSchema_NotStarted_DataRetrieval_Exception_Test()
        {
            // Arrange
            var templateCount = 1;
            var dataLakeName = RandomGenerator.Generate<string>();
            var validTemplate = true;

            //setup report file service: get schema list
            var schemaData = GenerateSchemaTemplateList(templateCount, dataLakeName, validTemplate, "#NotStarted");

            //set upload data command
            var uploadData = new DatastreamUploadCommand
            {
                SchemaList = schemaData.SchemaTemplateList,
                SchemaConfigDatatamp = schemaData.SchemaConfigDatastamp,
                RunMode = RunMode.TimerTrigger
            };

            //mock report file service: UpdateDatastamp
            _mockStorageService
                .Setup(_ => _.UpdateSchemaConfigDatastamp(
                    It.Is<string>(_ => _ == dataLakeName),
                    It.IsAny<string>()))
                .ReturnsAsync("OK")
                .Verifiable();

            //mock SysAid service: GetData
            var dataResponse = new DataResponseModel
            {
                Data = GenerateDataTable(),
                HasErrors = false,
                Runtime = RandomGenerator.Generate<double>()
            };

            _cgsDataService
                .Setup(_ => _.GetData(
                    It.IsAny<DataRequestModel>(),
                    It.IsAny<CancellationToken>()))
                .Throws(new Exception())
                .Verifiable();

            // Act
            _sut = new DatastreamService(_mockS3FileService.Object, _cgsDataService.Object, _mockStorageService.Object);
            var result = await _sut.UploadData(uploadData, default);

            // Assert
            Assert.IsInstanceOfType(result, typeof(List<string>));
            Assert.AreEqual(result.Count, 0);

            _cgsDataService.Verify(_ =>
                _.GetData(
                    It.IsAny<DataRequestModel>(),
                    It.IsAny<CancellationToken>()),
                    Times.Exactly(templateCount));

            _mockStorageService.Verify(_ =>
                _.UpdateSchemaConfigDatastamp(
                    It.IsAny<string>(),
                    It.IsAny<string>()),
                    Times.Exactly(templateCount));
        }

        #region Helpers

        private DataTable GenerateDataTable()
        {
            var dataTable = new DataTable();

            //set columns
            dataTable.Columns.Add("ID", typeof(string));
            dataTable.Columns.Add("SR_TYPE", typeof(string));
            dataTable.Columns.Add("COMPANY", typeof(string));
            dataTable.Columns.Add("STATUS", typeof(string));
            dataTable.Columns.Add("PROBLEM_TYPE", typeof(string));
            dataTable.Columns.Add("PROBLEM_SUB_TYPE", typeof(string));
            dataTable.Columns.Add("THIRD_LEVEL_CATEGORY", typeof(string));
            dataTable.Columns.Add("TITLE", typeof(string));
            dataTable.Columns.Add("REQUEST_USER", typeof(string));
            dataTable.Columns.Add("RESPONSIBILITY", typeof(string));
            dataTable.Columns.Add("PRIORITY", typeof(string));
            dataTable.Columns.Add("INSERT_TIME", typeof(string));
            dataTable.Columns.Add("DUE_DATE", typeof(string));
            dataTable.Columns.Add("CLOSE_TIME", typeof(string));

            //add test row
            dataTable.Rows.Add($"{RandomGenerator.Generate<int>()}", "Incident", "Lemongrass", "New", "Incident", "LCP", "Enigma", "Ascende Superius", "Isaac Newton", "Jaite Retuya", "P2 - High", "1538580331000", "1538659531000", "1538584489000");

            return dataTable;
        }

        #endregion Helpers
    }
}