﻿using CGSDatastream.Application.DataTransport.Commands.Requests;
using CGSDatastream.Application.Schema.Queries.Requests;
using LCP.OCTODashboard.CGSDatastream;
using LCP.OCTODashboard.CGSDatastream.Application.Enums;
using LCP.OCTODashboard.CGSDatastream.Application.Interfaces.Data;
using LCP.OCTODashboard.CGSDatastream.Application.Interfaces.Files;
using LCP.OCTODashboard.CGSDatastream.Application.Interfaces.Storage;
using LCP.OCTODashboard.CGSDatastream.Functions;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Timers;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace CGSDatastream.Test.Functions
{
    [TestClass]
    public class CGSDatastream_DataPullTest : TestBase
    {
        private CGSDatastreamFunction _sut;
        private readonly IServiceCollection _serviceCollection;
        private ILogger _logger;
        private Microsoft.Azure.WebJobs.ExecutionContext _executionContext;

        private readonly Mock<IStorageService> _mockStorageService;
        private readonly Mock<ICgsDataService> _mockDataService;
        private readonly Mock<IParquetFileService> _mockParquetFileService;
        private readonly Mock<IDatastreamService> _mockDatastreamService;

        private const string _functionName = "CG&S Datastream [timer trigger]";
        private static string _runMessage = $"{_functionName} run successfully.";

        private TimerInfo _timerInfo;
        public TimerSchedule Schedule { get; }
        public ScheduleStatus ScheduleStatus { get; }
        public bool IsPastDue { get; }

        public CGSDatastream_DataPullTest()
        {
            _serviceCollection = Startup.BuildContainer();

            _mockStorageService = CreateMock<IStorageService>();
            _mockDataService = CreateMock<ICgsDataService>();
            _mockParquetFileService = CreateMock<IParquetFileService>();
            _mockDatastreamService = CreateMock<IDatastreamService>();

            _serviceCollection.Replace(
                new ServiceDescriptor(typeof(IStorageService),
                _ => _mockStorageService.Object, ServiceLifetime.Transient));

            _serviceCollection.Replace(
                new ServiceDescriptor(typeof(ICgsDataService),
                _ => _mockDataService.Object, ServiceLifetime.Transient));

            _serviceCollection.Replace(
               new ServiceDescriptor(typeof(IParquetFileService),
               _ => _mockParquetFileService.Object, ServiceLifetime.Transient));

            _serviceCollection.Replace(
               new ServiceDescriptor(typeof(IDatastreamService),
               _ => _mockDatastreamService.Object, ServiceLifetime.Transient));

            Environment.SetEnvironmentVariable("AZUREWEBJOBSSTORAGE", RandomGenerator.Generate<string>());
            Environment.SetEnvironmentVariable("AZURE_STORAGE_BLOBNAME", RandomGenerator.Generate<string>());
            Environment.SetEnvironmentVariable("AZURE_STORAGE_SCHEMA_BLOBNAME", RandomGenerator.Generate<string>());
            Environment.SetEnvironmentVariable("AZURE_STORAGE_SCHEMA_FILENAME", RandomGenerator.Generate<string>());
            Environment.SetEnvironmentVariable("LCP_DB_CONNECTION_STRING", RandomGenerator.Generate<string>());
            Environment.SetEnvironmentVariable("RESET_HOUR", RandomGenerator.Generate<string>());
            Environment.SetEnvironmentVariable("AWS_ACCESS_ID", RandomGenerator.Generate<string>());
            Environment.SetEnvironmentVariable("AWS_ACCESS_KEY", RandomGenerator.Generate<string>());
            Environment.SetEnvironmentVariable("AWS_BUCKET_NAME", RandomGenerator.Generate<string>());
        }

        [TestInitialize]
        public void Init()
        {
            _logger = Mock.Of<ILogger>();
            _timerInfo = new TimerInfo(Schedule, ScheduleStatus, IsPastDue);
            _executionContext = new Microsoft.Azure.WebJobs.ExecutionContext { FunctionAppDirectory = "src" };
        }

        [TestMethod]
        public async Task Run_Datastream_TimerTrigger_OneSchema_CorrectTemplate_ParquetFile_Test()
        {
            // Arrange
            var schemaCount = 1;
            var validTemplate = true;
            var dataLakeName = "Customers";
            var preferredFileType = $"{FileType.Parquet}";
            var runMode = RunMode.TimerTrigger;
            Environment.SetEnvironmentVariable("PREFERRED_FILE_TYPE", preferredFileType);

            //setup report file service: get schema list
            var schemaDataResponse = GenerateSchemaTemplateList(schemaCount, dataLakeName, validTemplate);

            //mock storage service GetSchemaContents()
            _mockStorageService
                .Setup(_ => _.GetSchemaContents(It.IsAny<GetSchemaQuery>()))
                .ReturnsAsync((schemaDataResponse.SchemaTemplateList, schemaDataResponse.SchemaConfigDatastamp))
                .Verifiable();

            //set generated files
            var generatedFiles = new List<string>
            {
                $"cgs_{dataLakeName}_{DateTime.UtcNow:yyyy-MM-dd}.{preferredFileType}"
            };

            _mockParquetFileService
                .Setup(_ => _.UploadData(
                     It.Is<DatastreamUploadCommand>(_ =>
                        _.PreferredFileType == preferredFileType &&
                        _.SchemaList.Count == schemaCount &&
                        _.RunMode == runMode),
                    It.IsAny<CancellationToken>()))
                .ReturnsAsync(generatedFiles)
                .Verifiable();

            // Act
            _sut = new CGSDatastreamFunction(_serviceCollection.BuildServiceProvider(), "Testing");
            await _sut.Run(_timerInfo, _logger, _executionContext);

            // Assert            
            _mockStorageService.Verify(_ =>
               _.GetSchemaContents(
                   It.IsAny<GetSchemaQuery>()),
                   Times.Exactly(schemaCount));

            _mockParquetFileService.Verify(_ =>
                _.UploadData(
                    It.IsAny<DatastreamUploadCommand>(),
                    It.IsAny<CancellationToken>()),
                    Times.Exactly(schemaCount));
        }

        [TestMethod]
        public async Task Run_Datastream_TimerTrigger_OneSchema_CorrectTemplate_CsvFile_Test()
        {
            // Arrange
            var schemaCount = 1;
            var validTemplate = true;
            var dataLakeName = "Customers";
            var preferredFileType = $"{FileType.CSV}";
            var runMode = RunMode.TimerTrigger;
            Environment.SetEnvironmentVariable("PREFERRED_FILE_TYPE", preferredFileType);

            //setup report file service: get schema list
            var schemaDataResponse = GenerateSchemaTemplateList(schemaCount, dataLakeName, validTemplate);

            //mock storage service GetSchemaContents()
            _mockStorageService
                .Setup(_ => _.GetSchemaContents(It.IsAny<GetSchemaQuery>()))
                .ReturnsAsync((schemaDataResponse.SchemaTemplateList, schemaDataResponse.SchemaConfigDatastamp))
                .Verifiable();

            //set generated files
            var generatedFiles = new List<string>
            {
                $"cgs_{dataLakeName}_{DateTime.UtcNow:yyyy-MM-dd}.{preferredFileType}"
            };

            _mockDatastreamService
                .Setup(_ => _.UploadData(
                     It.Is<DatastreamUploadCommand>(_ =>
                        _.PreferredFileType == preferredFileType &&
                        _.SchemaList.Count == schemaCount &&
                        _.RunMode == runMode),
                    It.IsAny<CancellationToken>()))
                .ReturnsAsync(generatedFiles)
                .Verifiable();

            // Act
            _sut = new CGSDatastreamFunction(_serviceCollection.BuildServiceProvider(), "Testing");
            await _sut.Run(_timerInfo, _logger, _executionContext);

            // Assert            
            _mockStorageService.Verify(_ =>
               _.GetSchemaContents(
                   It.IsAny<GetSchemaQuery>()),
                   Times.Exactly(schemaCount));

            _mockDatastreamService.Verify(_ =>
                _.UploadData(
                    It.IsAny<DatastreamUploadCommand>(),
                    It.IsAny<CancellationToken>()),
                    Times.Exactly(schemaCount));
        }

        [TestMethod]
        public async Task Run_Datastream_TimerTrigger_OneSchema_InvalidTemplate_ParquetFile_Test()
        {
            // Arrange
            var schemaCount = 1;
            var validTemplate = false;
            var dataLakeName = "Customers";
            var preferredFileType = $"{FileType.Parquet}";
            var runMode = RunMode.TimerTrigger;
            Environment.SetEnvironmentVariable("PREFERRED_FILE_TYPE", preferredFileType);

            //setup report file service: get schema list
            var schemaDataResponse = GenerateSchemaTemplateList(schemaCount, dataLakeName, validTemplate);

            //mock storage service GetSchemaContents()
            _mockStorageService
                .Setup(_ => _.GetSchemaContents(It.IsAny<GetSchemaQuery>()))
                .ReturnsAsync((schemaDataResponse.SchemaTemplateList, schemaDataResponse.SchemaConfigDatastamp))
                .Verifiable();

            //set generated files
            var generatedFiles = new List<string>();

            _mockParquetFileService
                .Setup(_ => _.UploadData(
                     It.Is<DatastreamUploadCommand>(_ =>
                        _.PreferredFileType == preferredFileType &&
                        _.SchemaList.Count == schemaCount &&
                        _.RunMode == runMode),
                    It.IsAny<CancellationToken>()))
                .ReturnsAsync(generatedFiles)
                .Verifiable();

            // Act
            _sut = new CGSDatastreamFunction(_serviceCollection.BuildServiceProvider(), "Testing");
            await _sut.Run(_timerInfo, _logger, _executionContext);

            // Assert            
            _mockStorageService.Verify(_ =>
               _.GetSchemaContents(
                   It.IsAny<GetSchemaQuery>()),
                   Times.Exactly(schemaCount));

            _mockParquetFileService.Verify(_ =>
                _.UploadData(
                    It.IsAny<DatastreamUploadCommand>(),
                    It.IsAny<CancellationToken>()),
                    Times.Exactly(schemaCount));
        }

        [TestMethod]
        public async Task Run_Datastream_TimerTrigger_MultipleSchema_CorrectTemplate_ParquetFile_Test()
        {
            // Arrange
            var schemaCount = 100;
            var validTemplate = true;
            var dataLakeName = "Customers";
            var preferredFileType = $"{FileType.Parquet}";
            var runMode = RunMode.TimerTrigger;
            Environment.SetEnvironmentVariable("PREFERRED_FILE_TYPE", preferredFileType);

            //setup report file service: get schema list
            var schemaDataResponse = GenerateSchemaTemplateList(schemaCount, dataLakeName, validTemplate);

            //mock storage service GetSchemaContents()
            _mockStorageService
                .Setup(_ => _.GetSchemaContents(It.IsAny<GetSchemaQuery>()))
                .ReturnsAsync((schemaDataResponse.SchemaTemplateList, schemaDataResponse.SchemaConfigDatastamp))
                .Verifiable();

            //set generated files
            var generatedFiles = new List<string>();
            for (var t = 0; t < schemaCount; t++)
            {
                generatedFiles.Add($"cgs_{dataLakeName}_{DateTime.UtcNow:yyyy-MM-dd}.{preferredFileType}");
            }

            _mockParquetFileService
                .Setup(_ => _.UploadData(
                     It.Is<DatastreamUploadCommand>(_ =>
                        _.PreferredFileType == preferredFileType &&
                        _.SchemaList.Count == schemaCount &&
                        _.RunMode == runMode),
                    It.IsAny<CancellationToken>()))
                .ReturnsAsync(generatedFiles)
                .Verifiable();

            // Act
            _sut = new CGSDatastreamFunction(_serviceCollection.BuildServiceProvider(), "Testing");
            await _sut.Run(_timerInfo, _logger, _executionContext);

            // Assert            
            _mockStorageService.Verify(_ =>
               _.GetSchemaContents(
                   It.IsAny<GetSchemaQuery>()),
                   Times.Exactly(1));

            _mockParquetFileService.Verify(_ =>
                _.UploadData(
                    It.IsAny<DatastreamUploadCommand>(),
                    It.IsAny<CancellationToken>()),
                    Times.Exactly(1));
        }

        [TestMethod]
        public async Task Run_Datastream_TimerTrigger_MultipleSchema_CorrectTemplate_CsvFile_Test()
        {
            // Arrange
            var schemaCount = 100;
            var validTemplate = true;
            var dataLakeName = "Customers";
            var preferredFileType = $"{FileType.CSV}";
            var runMode = RunMode.TimerTrigger;
            Environment.SetEnvironmentVariable("PREFERRED_FILE_TYPE", preferredFileType);

            //setup report file service: get schema list
            var schemaDataResponse = GenerateSchemaTemplateList(schemaCount, dataLakeName, validTemplate);

            //mock storage service GetSchemaContents()
            _mockStorageService
                .Setup(_ => _.GetSchemaContents(It.IsAny<GetSchemaQuery>()))
                .ReturnsAsync((schemaDataResponse.SchemaTemplateList, schemaDataResponse.SchemaConfigDatastamp))
                .Verifiable();

            //set generated files
            var generatedFiles = new List<string>();
            for (var t = 0; t < schemaCount; t++)
            {
                generatedFiles.Add($"cgs_{dataLakeName}_{DateTime.UtcNow:yyyy-MM-dd}.{preferredFileType}");
            }

            _mockDatastreamService
                .Setup(_ => _.UploadData(
                     It.Is<DatastreamUploadCommand>(_ =>
                        _.PreferredFileType == preferredFileType &&
                        _.SchemaList.Count == schemaCount &&
                        _.RunMode == runMode),
                    It.IsAny<CancellationToken>()))
                .ReturnsAsync(generatedFiles)
                .Verifiable();

            // Act
            _sut = new CGSDatastreamFunction(_serviceCollection.BuildServiceProvider(), "Testing");
            await _sut.Run(_timerInfo, _logger, _executionContext);

            // Assert            
            _mockStorageService.Verify(_ =>
               _.GetSchemaContents(
                   It.IsAny<GetSchemaQuery>()),
                   Times.Exactly(1));

            _mockDatastreamService.Verify(_ =>
                _.UploadData(
                    It.IsAny<DatastreamUploadCommand>(),
                    It.IsAny<CancellationToken>()),
                    Times.Exactly(1));
        }

        [TestMethod]
        public async Task Run_Datastream_NoSchema_Test()
        {
            // Arrange

            //mock storage service GetSchemaContents()
            _mockStorageService
                .Setup(_ => _.GetSchemaContents(It.IsAny<GetSchemaQuery>()))
                .ReturnsAsync((new List<string>(), string.Empty))
                .Verifiable();

            // Act
            _sut = new CGSDatastreamFunction(_serviceCollection.BuildServiceProvider(), "Testing");
            await _sut.Run(_timerInfo, _logger, _executionContext);

            // Assert           
            _mockStorageService
               .Verify(_ => _.GetSchemaContents(It.IsAny<GetSchemaQuery>()), Times.Exactly(1));
        }
    }
}