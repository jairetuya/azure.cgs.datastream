# Introduction 
This repository contains the Azure function app (and corresponding config files) that extracts CG&S data of various types need to be streamed into the Data Lake (S3 bucket) in a configurable way. 

# Folder Structure
- config: contains the target environment variables needed for deployment
- src: contains all the CGSDatastream Azure function required files
- test: contains all the CGSDatastream Azure function unit test files

# High Level Application Overview
1. Runs on set run-time (cron) schedule - DAILY.
2. Reads schema files in a pre-determined Azure blob storage
   * Schema files are stored in a separate repo
     > https://lemongrassconsulting.visualstudio.com/OCTO%20Dashboards/_git/LCP.OCTODashboard.CGSSchemaFiles
   * If this fails, you need to be granted access to the blob storage
3. Interprets the schema file to compose the SQL needed to read the target table   
4. Retrieves the table-data in the LCP CG&S database
   * Uses a connection string (change the value accordingly to target the correct database)
5. Generates the pre-determined (configurable) preferred file type.
6. Uploads the data/file into a pre-determined S3 bucket.


# Getting Started
1.	How to set-up and run locally
	- Retrieve latest of this repo locally. Make sure that the solution builds successfully.
	- Check the local.settings.json and verify all values are updated accordingly.
	- Run 'CGSDatastream'. It's advisable to place a Break on CGSDatastream_DataPull.cs file.

2.	Software dependencies
	* No software dependencies 
	* Does not require pre-installation software

# Build and Test
If the preferred 'PREFERRED_FILE_TYPE' is a 
	- PARQUET, you need a Parquet file viewer such as Apache Parquet Viewer to open and read the file;
	  > Apache Parquet Viewer: https://www.microsoft.com/en-us/p/apache-parquet-viewer/9pgb0m8z4j2t
	- CSV, you need a text view such a Notepad.
	If the preferred file is unreadable that means this function was updated wrongly.

# Deployment
The two serverless.yml files (Serverless Framework): 
	- serverless_aws.yml - is used to create the AWS S3 bucket
	  * You need access to an AWS account
	- serverless_azure.yml - is used to create the Azure function app.
	  * You need acess to an Azure portal

# Contribute
This function app can be enhanced by implementing CQRS pattern.